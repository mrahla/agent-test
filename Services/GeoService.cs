﻿using Google.Maps;
using System;
using System.Collections.Generic;
using Google.Maps.DistanceMatrix;

namespace Services
{
    public static class GeoService
    {
        private static readonly string _googlemapsKey = "";

        public static long CalculateDistance(string origin, string distance)
        {
            GoogleSigned.AssignAllServices(new GoogleSigned(_googlemapsKey));
            var request = new DistanceMatrixRequest
            {
                //WaypointsDestination = new List<Location> { new LatLng(49.171020M, 5.969358M) },
                WaypointsOrigin = new List<Location> { origin },
                WaypointsDestination = new List<Location> { distance },
            };

            var response = new DistanceMatrixService().GetResponse(request);

            if (response.Status == ServiceResponseStatus.Ok && response.Rows != null && response.Rows.Length > 0)
            {
                var result = response.Rows[0];
                if (result != null && result.Elements != null && result.Elements.Length > 0 && result.Elements[0].distance != null)
                {
                    return result.Elements[0].distance.Value;
                }
            }
            return -1;
        }
    }
}
