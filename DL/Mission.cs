﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DL
{
    public class Mission
    {
        public string Country { get; set; }
        public string Address { get; set; }
        public DateTime Date { get; set; }
    }
}
