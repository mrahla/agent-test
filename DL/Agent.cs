﻿using LiteDB;
using System;
using System.Collections.Generic;

namespace DL
{
    public class Agent
    {
        [BsonId]
        public string Id { get; set; }
        public IEnumerable<Mission> Missions { get; set; }
    }
}
