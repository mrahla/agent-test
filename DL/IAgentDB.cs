﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DL
{
    interface IAgentDB
    {
        void PrepopulateDB(IEnumerable<Agent> agents);
        void AddAgent(Agent agent);
        Agent GetAgent(string id);
        IEnumerable<Agent> FindAgentsWithMissions(int count = -1);
        string FindClosestMission(string address);
    }
}
