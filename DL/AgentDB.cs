﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DL
{
    public class AgentDB : IAgentDB
    {
        private static readonly string dbPath = @"C:\Temp\agents.db";

        public void AddAgent(Agent agent)
        {
            if (agent == null)
                throw new ArgumentNullException(nameof(agent), "Agent is null");
            using (var db = new LiteDatabase(@"C:\Temp\agentsDB.db"))
            {
                var col = db.GetCollection<Agent>("agents");
                Agent a = GetAgent(agent.Id);
                if (a != null)
                {
                    a.Missions = a.Missions ?? Enumerable.Empty<Mission>();
                    a.Missions.Concat(agent.Missions);
                }
                else
                {
                    col.Insert(agent.Id, agent);
                }
            }
        }

        public IEnumerable<Agent> FindAgentsWithMissions(int count = -1)
        {
            using (var db = new LiteDatabase(dbPath))
            {
                var col = db.GetCollection<Agent>("agents");
                col.EnsureIndex(a => a.Missions, "$.Missions[*]");
                return count > -1 
                    ? col.Find(x => x.Missions != null).Where(a => a.Missions.Count() == count).ToList()
                    : col.Find(x => x.Missions != null).ToList();
            }
        }

        public string FindClosestMission(string address)
        {
            throw new NotImplementedException();
        }

        public Agent GetAgent(string id)
        {
            using (var db = new LiteDatabase(dbPath))
            {
                var col = db.GetCollection<Agent>("agents");
                col.EnsureIndex(a => a.Id, true);
                return col.FindById(id);
            }
        }

        public void PrepopulateDB(IEnumerable<Agent> agents)
        {
            using (var db = new LiteDatabase(dbPath))
            {
                var col = db.GetCollection<Agent>("agents");
                col.InsertBulk(agents);
            }
        }
    }
}
