﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agentApi.Models;
using DL;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace agentApi.Controllers
{
    //[Route("api/[controller]")]
    [Route("/")]
    [ApiController]
    public class AgentController : ControllerBase
    {
        private static readonly AgentDB _db = new AgentDB();

        private readonly IHostingEnvironment _hostingEnvironment;

        public AgentController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public string Get()
        {
            return "Hi Agent!";
        }

        [HttpPost(template: "/mission")]
        public void PostMission([FromBody] APIAgent agent)
        {
            if (agent != null)
            {
                Agent a = new Agent
                {
                    Id = agent.Agent,
                    Missions = new List<Mission>
                    {
                        new Mission
                        {
                            Address = agent.Address,
                            Country = agent.Country,
                            Date = agent.Date
                        }
                    }
                };
                _db.AddAgent(a);
            }
        }

        [HttpGet(template: "/countries-by-isolation")]
        public KeyValuePair<string, int> GetIsolatedCountry()
        {
            IEnumerable<Agent> agents = _db.FindAgentsWithMissions(1);
            Dictionary<string, int> d = new Dictionary<string, int>();
            foreach (var m in agents.SelectMany(a => a.Missions))
            {
                if (!d.ContainsKey(m.Country)) { d[m.Country] = 1; }
                else { d[m.Country] += 1; }
            }
            d.OrderByDescending(a => a.Value);
            return d.FirstOrDefault();
        }

        [HttpPost(template: "/find-closest")]
        public Mission Post([FromBody] string address)
        {
            Mission m = new Mission();
            IEnumerable<Agent> agents = _db.FindAgentsWithMissions();
            var missions = agents.SelectMany(a => a.Missions).ToList();
            if (missions != null && missions.Any())
            {
                m = missions.First();
                long min = Services.GeoService.CalculateDistance(address, m.Address);
                if (missions.Skip(1).Any())
                {
                    for (int i = 1; i < missions.Count(); i++)
                    {
                        long distance = Services.GeoService.CalculateDistance(address, m.Address);
                        if (distance < min)
                        {
                            min = distance;
                            m = missions[i];
                        }
                    } 
                }
            }

            return m;
        }

        [HttpGet("/prepop")]
        public bool Prepopulate()
        {
            var data = JsonConvert.DeserializeObject<IEnumerable<APIAgent>>(System.IO.File.ReadAllText(_hostingEnvironment.ContentRootPath + "/data.json"));
            if (data != null && data.Any())
            {
                var d = new Dictionary<string, Agent>();
                foreach (var agent in data)
                {
                    var m = new List<Mission> { new Mission
                    {
                        Address = agent.Address,
                        Country = agent.Country,
                        Date = agent.Date
                    }};

                    if (d.ContainsKey(agent.Agent))
                    {
                        d[agent.Agent].Missions = d[agent.Agent].Missions.Concat(m);
                    }
                    else
                    {
                        d[agent.Agent] = new Agent { Id = agent.Agent, Missions = m };
                    }
                }
                _db.PrepopulateDB(d.Values);
                return true;
            }
            return false;
        }

    }
}
