using DL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AgentTest
{
    [TestClass]
    public class DBTest
    {
        private readonly AgentDB _db;

        public DBTest()
        {
            _db = new AgentDB();
        }

        [TestMethod]
        public void AddGetTest()
        {
            var agent = _db.GetAgent("007");
            Assert.IsNotNull(agent);
        }
    }
}
